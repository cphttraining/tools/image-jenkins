# tool-jenkins

基於 [官方文件: Installing Jenkins (Docker)](https://www.jenkins.io/doc/book/installing/docker/) 的步驟提供 Docker 映像檔，並包成 `docker-compose.yml`

## 安裝
1. 下載此專案
   ```sh
   $ git clone https://gitlab.com/cphtofficial/tool-jenkins.git

   $ cd tool-jenkins
   ```
2. 啟動容器
   ```sh
   $ docker compose up -d
   ```